package AdminCab;
use Mojo::Base 'Mojolicious';
use DBIx::Custom;
use Mojo::JSON qw(decode_json encode_json);

my $config = '';

has 'dbh' => sub {
	my $self = shift;
	
	my $dbi = DBIx::Custom->connect(
			dsn => $config->{dsn},
			user => $config->{user},
			password => $config->{password},
			option => {mysql_enable_utf8 => 1}
	);
	$dbi->execute('SET NAMES utf8');

	return $dbi;
};

# This method will run once at server start
sub startup {
	my $self = shift;

	#Reading config file
	$config = $self->plugin('Config');
	
	$self->mode($config->{mode});
	$self->secrets($config->{secrets});

	$self->helper(notify => sub {
		$self->app->dbh->insert(
		[{
			profile_type => $_[1],
			profile_id => $_[2],
			message => $_[3],
		}],
		table => 'notifications',
		ctime => 'regdate',	
		); 
	});

    $self->helper(req_fmt => sub {
        sprintf("%04s-Е",$_[1]);
    });

  # Router
  my $r = $self->routes;

  $r->get('/')->to('requests#requests');

  $r->get('/users')->to('users#users');
  $r->get('/users/:profile_id')->to('users#view');

  $r->get('/users/:profile_id/requests/')->to('requests#list');
  $r->get('/users/:profile_id/requests/:request_id')->to('requests#view');
  $r->get('/users/:profile_id/requests/:request_id/print')->to('requests#print');
  $r->get('/users/:profile_id/requests/:request_id/progress/')->to('requests#progress');
  $r->post('/users/:profile_id/requests/:request_id/progress/')->to('requests#comment');
  $r->post('/users/:profile_id/requests/:request_id/share/')->to('requests#share');
  $r->get('/users/:profile_id/requests/:request_id/supplier/')->to('requests#supplier_notify');

  $r->get('/requests/')->to('requests#requests');
  $r->post('/requests/')->to('requests#filter');

  $r->post('/search/')->to('search#results');

  #Convert data-structure (set develpment mode in config file)
  $r->get('/convert/')->to('requests#convert');

  $self->hook(before_dispatch => sub {

		my $self = shift;
		
		$self->req->url->base(Mojo::URL->new($config->{url}));

  }) if $self->mode eq 'production';
}

1;
