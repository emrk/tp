package AdminCab::Search;
use Mojo::Base 'Mojolicious::Controller';

sub results {
	my $self = shift;

	my $params = $self->req->params->to_hash;
	my %source = (
		'users' => { 
			table => 'profiles',
			search_field => 'register_code',
			type => 'Профиль',
			result_fields => [qw/id/],
		},
		'requests' => { 
			table => 'requests',
			search_field => 'id',
			type => 'Заявление',
			result_fields => [qw/id profile_id/],
		},
	);

	my $results = $self->app->dbh->select(
		table => 'requests',
		column => ['id','profile_id'],
		where => {'id' => $params->{'register_code'}},
	)->fetch_hash_all;
	
	$self->render(
		results => $results,
		search_query => $params->{register_code},
	);
}

1;
