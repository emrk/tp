package AdminCab::Users;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw(decode_json encode_json);


sub users {

	my $self = shift;
	
	my $users = $self->app->dbh->select(
		column => [
			'id',
			'fuel_name',
			'register_code',
			'type',
			'regdate',
		],
		table => 'profiles',
		where => {pfl_status => 0},
	)->fetch_hash_all;
	
	$self->render(users => $users);
}

sub view {
  my $self = shift;
  my $profile_id = $self->param('profile_id') || undef;
  my $profile = defined;

  
  $profile = $self->app->dbh->select(
	table => 'profiles',
	where => {id => $profile_id},
  )->fetch_hash;

  if($profile){
	  my $pfl_content = $profile->{pfl_content};
	  utf8::encode($pfl_content);
  
	  if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

	  $self->render(
		profile => $profile,
		pfl_content => $pfl_content,
		template => 'users/'.$profile->{type},
	  );
  }else{
	$self->render(
		message => 'User profile not finded',
		template => 'notification',
	);
	
  };
}

1;
