package AdminCab::Requests;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw(decode_json encode_json);

my $push_msg = {};

sub requests {
  my $self = shift;
  my $answers = $self->param('answers') || undef;
  my $requests;

  if($answers){
	$requests = $self->app->dbh->select(
		column => [
			{'requests' => [qw/id regdate profile_id/]},
			{'profiles' => [qw/fuel_name/]}
		],
	    table => 'profiles',
		join => [
			'inner join requests on requests.profile_id = profiles.id',
			'inner join progress on progress.request_id = requests.id',
		],
   		where => {answer_flag => 1},
		append => 'group by request_id',
	)->fetch_hash_all;

  }else{	
	$requests = $self->app->dbh->select(
		column => [
			{ requests => [qw/id regdate profile_id/] },
			{ profiles => [qw/fuel_name/]},
		],
		table => 'requests',
		join => ['inner join profiles on profiles.id = requests.profile_id'],
		where => 'status_id is null ',
	)->fetch_hash_all;

  };

  my $statuses = $self->app->dbh->select(
	table => 'statuses',
  )->fetch_hash_all;

  my $count_answers = $self->app->dbh->select(
	table => 'progress',
	column => 'count(request_id)',
	where => {answer_flag => 1},

  )->value;

  $self->render(
	requests => $requests,
	statuses => $statuses,
	status_id => 0,
	count_answers => $count_answers,
	answers => $answers,
  );
}

sub filter {
	my $self = shift;
	my $status_id = $self->param('status_id') || 0;

	my $requests = $self->app->dbh->select(
		column => [
			{ 'requests' => [qw/id regdate profile_id/] },
			{ 'profiles' => [qw/fuel_name/]},
		],
		table => 'requests',
		join => ['inner join profiles on profiles.id = requests.profile_id'],
		where => {status_id => $status_id},
	)->fetch_hash_all;

	my $statuses = $self->app->dbh->select(
		table => 'statuses',
	)->fetch_hash_all;

  	my $count_answers = $self->app->dbh->select(
		table => 'progress',
		column => 'count(answer_flag)',
		where => {answer_flag => 1},
	)->value;
	
	$self->render(
		requests => $requests,
		statuses => $statuses,
		status_id => $status_id,
		template => '/requests/requests',
		count_answers => $count_answers,
  );
}

sub list {
  my $self = shift;
  my $profile_id = $self->stash('profile_id');

  my $requests = $self->app->dbh->select(
	column => [{ requests => [qw/id regdate profile_id/] }],
	table => 'requests',
	where => {profile_id => $profile_id},
  )->fetch_hash_all;
	
  $self->render(
	requests => $requests,
	template => '/users/requests',
  );
}

sub progress {
  my $self = shift;
  my $request_id = $self->stash('request_id');
  my $show_answer = $self->param('show_answer');

  my $statuses = $self->app->dbh->select(
	table => 'statuses',
	column => ['id','caption'],
  )->kv;

  $self->app->dbh->update(
	{answer_flag => 2},
	table => 'progress',
	where => {id => $show_answer},
  );

  my $progress = $self->app->dbh->select(
	table => 'progress',
	where => {request_id => $request_id},
  )->fetch_hash_all;

  foreach my $key(@{$progress}){
	if($key->{pgs_content}){
		my $pgs_content = $key->{pgs_content};
  		utf8::encode($pgs_content);
		$key->{pgs_content} = decode_json($pgs_content);
	};
  };

  $self->render(
	progress => $progress,
	statuses => $statuses,
  );
}

sub view {
  my $self = shift;
  my $request_id = $self->stash('request_id');
  my $alias = $self->config->{'alias'};
  my $template_version = $self->param('template_version') || undef;

  my $request = $self->app->dbh->select(
	table => 'requests',
	where => {
		id => $request_id,
	},
  )->fetch_hash || undef;

  if(!defined($request)){
	
	return $self->render(
		message => 'Заявление не найдено', 
		template => 'notification',		
		);
  };
  
  my $req_content = $request->{req_content};
  utf8::encode($req_content);

  my $pfl_content = $request->{pfl_content};
  utf8::encode($pfl_content);
  
  if (!defined($req_content = decode_json($req_content))) { say 'Error: ', '$json->error' };
  if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

  my $suppliers = $self->app->dbh->select(
		table => 'suppliers',
		column => ['id','caption'],
  )->fetch_all; 

  my $template = $req_content->{req_template};
  $template = "archive/$template_version/$req_content->{req_template}" if($template_version);

  $self->render(
	request => $request,
	req_content => $req_content,
	pfl_content => $pfl_content,
	suppliers => $suppliers,
	template => "requests/tmpl/$alias/$pfl_content->{type}/view/$template"
  );
}

sub print {
  my $self = shift;
  my $request_id = $self->stash('request_id');
  my $request = undef;
  my $alias = $self->config->{'alias'};
  my $template_version = $self->param('template_version') || undef;
  my $share_link = ''; #Patch for printing template compability;  

  $request = $self->app->dbh->select(
	table => 'requests',
	where => {
		id => $request_id,
	},
  )->fetch_hash || undef;

  if(!defined($request)){
	
	return $self->render(
		message => 'Заявление не существует',
		status_class => 'warning',
		template => 'notification',		
		);
  };

  my $statuses = $self->app->dbh->select(
        table => 'statuses',
        column => ['id','caption'],
  )->kv;

  my $req_content = $request->{req_content};
  utf8::encode($req_content);

  my $pfl_content = $request->{pfl_content};
  utf8::encode($pfl_content);
  
  if (!defined($req_content = decode_json($req_content))) { say 'Error: ', '$json->error' };
  if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

  my $template = $req_content->{req_template};
  $template = "archive/$template_version/$req_content->{req_template}" if($template_version);

  $self->render(
	request => $request,
	req_content => $req_content,
	pfl_content => $pfl_content,
	statuses => $statuses,
	share_link => $share_link,
	template => "requests/tmpl/$alias/$pfl_content->{type}/print/$template"
  );
}

sub comment{
	my $self = shift;

	my $profile_id = $self->stash('profile_id');
	my $request_id = $self->stash('request_id');
	my $comment_content = $self->req->params->to_hash;
    my $upload_status = 0;
    my $file_num = 0;

    foreach my $upload (@{$self->req->every_upload('attach')}){

    	if($upload && $upload->filename && $upload_status == 0){
        	if ($upload->size > 10240000){

            	$upload_status = 1;
                $push_msg = {
                	type => 'warning',
                    message => 'Файл '.$upload->filename.' не загружен. Размер файла больше  10Мб.'
                };

           	};

		  	my $file_ext = ''; #File extention
			my $file_ct = ''; #Content-type header
			if (exists $self->config->{'content_types'}{$upload->headers->content_type}){
				$file_ext = $self->config->{'content_types'}{$upload->headers->content_type};
				$file_ct = $upload->headers->content_type;

			};

			if(!$file_ext){
        
            	$upload_status = 1;
                $push_msg = {
                	type => 'warning',
                    message => 'Файл '.$upload->filename.' не загружен. Данный тип файлов не поддерживается.'
                };

          	};
    
            if ($upload_status == 0){
            	my $file_caption = $upload->filename;
 
            	my $filename = $request_id.time.$profile_id.$file_num.'.'.$file_ext;
                $upload->move_to($self->config->{'uploads'}."/progress/$filename");

                push @{$comment_content->{'comment_attach'}}, [$filename, $file_caption, $file_ct];
          	};
     	};
        $file_num++;
	};
    
    if($upload_status == 0){

		$comment_content->{'token'} = rand(999);
		my $comment_content_enc = encode_json($comment_content);

		$self->app->dbh->insert(
			{
				status_id => $comment_content->{'status_id'},
				request_id => $request_id,
				pgs_content => $comment_content_enc
			},
			ctime => 'regdate',
			table => 'progress',
		);
		
		#update request status
		$self->app->dbh->update(
			{status_id => $comment_content->{'status_id'}},
			table => 'requests',
			where => {id => $request_id},
		);

		$push_msg = {type => 'success', message => "Статус заявления ". $self->app->req_fmt($request_id)." обновлен"};
		$self->app->notify('user', $profile_id, $push_msg->{'message'});
	};

	$self->flash(
        push_msg => $push_msg,
    );

	$self->redirect_to('progress');
}

sub share{
	my $self = shift;
	my $request_id = $self->stash('request_id');
	my $link = $self->param('link') || '';
	my $share_limit = 'NULL';
	
	if($link){
		$link = '';
		$share_limit = "NULL";
	}else{
		my @dict = (0..9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','$','!');
		for (my $i = 0; $i<50; $i++){
			$link = $link.$dict[rand(63)];
		};
	
		$push_msg = {
			type => 'success',
			message => "Ссылка успешно создана.",
		};

		if ($self->config->{'share_limit'} > 0){
			$share_limit = "NOW() + interval ".$self->config->{'share_limit'}." day";
			$push_msg->{'message'} = $push_msg->{'message'}." Срок действия ссылки ".$self->config->{'share_limit'}." дней";

		};	

		$self->flash(
			push_msg => $push_msg
		);
	};

	$self->app->dbh->update(
		{
			share_link => $link,
			share_limit => \$share_limit,
		},
		table => 'requests',
		where => {id => $request_id},
	);

	$self->redirect_to('.');
};

sub supplier_notify{
	my $self = shift;
	my $request_id = $self->stash('request_id');
	my $supplier_id = $self->param('id') || 0;

	my $request_status = $self->app->dbh->select(
		table => 'requests',
		column => ['caption','share_link','share_limit'],
		where => {'requests.id' => $request_id},
		join => ['inner join statuses on statuses.id=requests.status_id'],
	)->fetch_hash;

	my $supplier_exists = $self->app->dbh->select(
		table => 'suppliers',
		column => 'caption',
		where => {id => $supplier_id},
	)->value || undef;

	if($request_status && $supplier_exists){
		my $message = "Заявление ".$self->app->req_fmt($request_id)."\nСтатус заявления \"".$request_status->{'caption'}."\"\nСсылка на заявление ".$self->config->{'url'}."/share/$request_status->{'share_link'}/, доступ открыт до $request_status->{'share_limit'}";

		$self->app->notify('supplier', $supplier_id, $message);
		$push_msg = {
			'type' => 'success',
			'message' => "Уведомление в адрес $supplier_exists направлено",
		};
	}else{
		$push_msg = {
			type => 'danger',
			message => "Ошибка отправки уведомления. Заявление не найдено либо поставщик отсутствует",
		};	
	};

	$self->flash(
		push_msg => $push_msg,
	);
	
	$self->redirect_to('.');
}

sub convert{
	my $self = shift;

	return $self->render(text => 'Not enabled in production mode') if($self->config->{'mode'} eq 'production');

	my $table = $self->param('table');	
	my $requests = $self->app->dbh->select(
		table => $table,
		column => ['id','pfl_content'],
	)->fetch_all;

	my $result = ();
	my %content_types = (
	 'jpeg' => 'image/jpeg',
	 'pdf' => 	'application/pdf', 
	);
	foreach my $req_content (@{$requests}){
		utf8::encode($req_content->[1]);
		my $req_content_dec = decode_json($req_content->[1]);
		

		if($req_content_dec->{'certificate_file'}){
			my $file_ext = $req_content_dec->{'certificate_file'};
			$file_ext =~ s/^.+\.//;
			$req_content_dec->{'certificate_file'} = [$req_content_dec->{'certificate_file'}, $content_types{$file_ext}];
			$result->{$req_content->[0]} = $req_content_dec->{'certificate_file'};	
		};


		my $req_content_enc = encode_json($req_content_dec);
		$self->app->dbh->update(
			{'pfl_content' => $req_content_enc},
			table => $table,
			where => {id => $req_content->[0]},
		);
	
	};

	$self->render(
		message => $result,
		template => 'development.notification',
	);
}

1;
