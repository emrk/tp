package PersCab::Profile;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw(decode_json encode_json);

sub view {
  my $self = shift;
  my $profile_id = $self->session->{id} || undef;

  return $self->redirect_to('/') if(!defined($profile_id));
  
  my $profile = $self->app->dbh->select(
	table => 'profiles',
	where => {id => $profile_id},
  )->fetch_hash_one;

  if($profile){

	  my $pfl_content = $profile->{pfl_content};
	  utf8::encode($pfl_content);
  
	  if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

	  $self->render(
		profile => $profile,
		pfl_content => $pfl_content,
		template => '/profile/'.$profile->{type},
	  );
  }else{
	$self->redirect_to('/');

  };
}

sub update {
	my $self = shift;

	my $profile = $self->req->params->to_hash;
	my $profile_id = $self->session->{id} || undef;

	return $self->redirect_to('/') if(!defined($profile_id));

	my $push_msg = {};
	my $upload_status = 0;
	my $upload = $self->req->upload('upload_file') || undef;

	if($upload && $upload->filename && $upload_status == 0){
    	if ($upload->size > 10240000){
		 	$upload_status = 1;
            $push_msg = {
            	type => 'warning',
                message => 'Файл '.$upload->filename.' не загружен. Размер файла превышает 10мБ.'
            };

       	};

        my $file_ext = ''; #File extention
        my $file_ct = ''; #Content-type
        if (exists $self->config->{'content_types'}{$upload->headers->content_type}){
        	$file_ext = $self->config->{'content_types'}{$upload->headers->content_type};
            $file_ct = $upload->headers->content_type;

        };

        if(!$file_ext){
        	$upload_status = 1;
            $push_msg = {
            	type => 'warning',
                message => 'Файл '.$upload->filename.' не загружен. Тип файла не поддерживается.'
        	};

		};


    	if ($upload_status == 0){
        	my $filename =  time.$profile_id.'.'.$file_ext;
        	$upload->move_to($self->config->{'uploads'}."/certificates/$filename");

        	$profile->{'certificate_file'} = "$filename:$file_ct";

		};
    };

	if($profile->{'certificate_file'}){#Checking for certfile value
		$profile->{'certificate_file'} = [split(':',$profile->{'certificate_file'})];

	};

	my $profile_enc = encode_json($profile);

	if($upload_status == 0){

		my $en_pass = $self->app->dbh->select(
			table => 'profiles',
			column => ['password'],
			where => {id => $profile_id},	
		)->value || undef;
		
		$self->app->dbh->update(
			{pfl_content => $profile_enc},
			table => 'profiles',
			where => {id => $profile_id},
		);

		$push_msg = {
			type => 'success',
			message => 'Профиль успешно обновлён',

		};

		$self->app->notify('user', $profile_id, $push_msg->{'message'}, 2);

	};
	
	$self->flash(
		push_msg => $push_msg,
		profile => $profile,
	);

	$self->redirect_to('/profile/');
}

sub auth {
	my $self = shift;
	my $register_code = $self->param('rcode');
	my $password = $self->param('pass');
	my $profile_id = $self->session->{id} || undef;

	if (!$profile_id && ($register_code && $password)){

		my $profile = $self->app->dbh->select(
			table => 'profiles',
			columns => ['id','password','status'],
			where => {register_code => $register_code},	
		)->one || undef;

		my $en_pass = $profile->{password};


		if($en_pass eq $password){

				$profile_id = $profile->{id};

		};
	};

	if($profile_id){
		$self->session->{id} = $profile_id;
		$self->redirect_to('/requests/');
	};
}

sub logout {
	my $self = shift;
	$self->session(expires => 1);
	$self->redirect_to('/');
}

sub recovery {
	my $self = shift;
	my $register_code = $self->param('rcode');
	
	if($register_code){

		my $profile = $self->app->dbh->select(
			table => 'profiles',
			columns => ['id','password'],
			where => {register_code => $register_code},
		)->one || undef;

		$self->app->notify('user', $profile->{id},"Пароль: $profile->{password}") if($profile->{id}); 
	
		my $push_msg = {
			type => 'success',
			message => 'Пароль восстановлен на указанные в профиле email и мобильный телефон.',
		};

		$self->flash(
			push_msg => $push_msg,
		);

		$self->redirect_to('/');
	};
}

1;
