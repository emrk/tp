package PersCab::Requests;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw(decode_json encode_json);

sub requests {
	my $self = shift;
	my $profile_id = $self->session->{id} || undef;
	
  	return $self->redirect_to('/') if(!defined($profile_id));

	my $requests = $self->app->dbh->select(
		table => 'requests',
		where => {profile_id => $profile_id},
 	)->fetch_hash_all;

	my $statuses = $self->app->dbh->select(
		table => 'statuses',
		column => ['id','type'],
	)->kv;

	$self->render(
		requests => $requests,
		statuses => $statuses,
	);
}

sub progress {
  	my $self = shift;
  	my $request_id = $self->stash('request_id');
  	my $profile_id = $self->session->{id} || undef;

  	return $self->redirect_to('/') if(!defined($profile_id));

  	my $statuses = $self->app->dbh->select(
		table => 'statuses',
		column => ['id','caption'],
  	)->kv;

  	my $progress = $self->app->dbh->select(
		table => 'progress',
		column => [{progress => [qw/id regdate status_id pgs_content answer_flag/]}],
		where => {request_id => $request_id, profile_id => $profile_id},
		join => ['inner join requests on requests.id = progress.request_id'],
		append => 'and progress.status_id is not null order by progress.regdate',
  	)->fetch_hash_all;

  	foreach my $key(@{$progress}){
    	if($key->{'progress.pgs_content'}){
        	my $pgs_content = $key->{'progress.pgs_content'};
        	utf8::encode($pgs_content);
        	$key->{'progress.pgs_content'} = decode_json($pgs_content);
    	};
  	};

  	$self->render(
		progress => $progress,
		statuses => $statuses,
  	);
}

sub answer {
    my $self = shift;
	my $push_msg = {};

    my $profile_id = $self->session->{id} || undef;
    my $request_id = $self->stash('request_id');

  	return $self->redirect_to('/') if(!defined($profile_id));

    my $answer_content = $self->req->params->to_hash;
	my $answer_flag = 0;
	my $upload_status = 0;
	my $file_num = 0;

	#Check for permission
	my $pgs_content = $self->app->dbh->select(
		table => 'progress',
    	column => ['pgs_content'],
	    where => {request_id => $request_id, profile_id => $profile_id, 'progress.id' => $answer_content->{'progress_id'}},
    	join => ['inner join requests on requests.id = progress.request_id'],
	)->fetch_hash_one || undef;

	if($pgs_content){

		utf8::encode($pgs_content->{'pgs_content'});
		my $pgs_content_dec = decode_json $pgs_content->{'pgs_content'};

		if($pgs_content_dec->{'token'} == $answer_content->{'pgs_content_token'} && $answer_content->{'answer'}){

			foreach my $upload (@{$self->req->every_upload('attach')}){

				if($upload && $upload->filename && $upload_status == 0){
					if ($upload->size > 10240000){

						$upload_status = 1;
						$push_msg = {
							type => 'warning', 
							message => 'Файл '.$upload->filename.' не загружен. Размер файла превышает 10мБ.'
						};

					};
					
					my $file_ext = ''; #File extention
					my $file_ct = ''; #Content-type
		            if (exists $self->config->{'content_types'}{$upload->headers->content_type}){
        		        $file_ext = $self->config->{'content_types'}{$upload->headers->content_type};
                		$file_ct = $upload->headers->content_type;

            		};

		            if(!$file_ext){
						$upload_status = 1;
						$push_msg = {
							type => 'warning', 
							message => 'Файл '.$upload->filename.' не загружен. Тип файла не поддерживается.'
						};

					};
	
					if ($upload_status == 0){
						my $file_caption = $upload->filename;
						my $filename =  $answer_content->{'progress_id'}.time.$profile_id.$file_num.'.'.$file_ext;
						$upload->move_to($self->config->{'uploads'}."/progress/$filename");
	
						push @{$pgs_content_dec->{'answer_attach'}}, [$filename, $file_caption, $file_ct];
		
					};	
				};
				$file_num++;
			};

			if($upload_status == 0 && $answer_content->{'answer'}){
				$pgs_content_dec->{'answer'} = $answer_content->{'answer'};
				$pgs_content_dec->{'ctime'} = time();
				$answer_flag = 1; #Closing answer form

			    my $pgs_content_enc = encode_json($pgs_content_dec);

				$self->app->dbh->update(
	        		{
						pgs_content => $pgs_content_enc,
						answer_flag => $answer_flag,
					},
		    	    table => 'progress',
    		    	where => {id => $answer_content->{'progress_id'}},
	   			);

				$push_msg = {'type' => 'success', 'message' => 'Сообщение доставлено'};
	
			};
	
		}else{
		
			$push_msg = {'type' => 'warning', 'message' => 'Сообщение не доставлено, отсутствует текст'};

		};
	};

	$self->flash(
		push_msg => $push_msg,
	);

    $self->redirect_to('.');

}

sub print {
	my $self = shift;
  	my $request_id = $self->stash('request_id');
  	my $profile_id = $self->session->{id} || undef;
  	my $request = undef;
  	my $alias = $self->config->{'alias'};	
  	my $template_version = $self->param('template_version') || undef;

  	return $self->redirect_to('/') if(!defined($profile_id));
  
  	$request = $self->app->dbh->select(
		table => 'requests',
		where => {
			id => $request_id,
			profile_id => $profile_id,
		},
	  )->fetch_hash || undef;

	if(!defined($request)){
	
		return $self->render(
			msg => 'Заявление не существует.',
			status_class => 'warning',
			template => 'notification',		
			);
	};

 	my $statuses = $self->app->dbh->select(
    	table => 'statuses',
		column => ['id','caption'],
  	)->kv; 
  
	my $req_content = $request->{req_content};
  	utf8::encode($req_content);

  	my $pfl_content = $request->{pfl_content};
  	utf8::encode($pfl_content);
  
  	if (!defined($req_content = decode_json($req_content))) { say 'Error: ', '$json->error' };
  	if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

  	my $template = $req_content->{req_template};
  	$template = "archive/$template_version/$req_content->{req_template}" if($template_version);

  	$self->render(
		request => $request,
		req_content => $req_content,
		pfl_content => $pfl_content,
		statuses => $statuses,
		share_link => '',
		template => "requests/tmpl/$alias/$pfl_content->{type}/print/$template"
  	);
}

sub add {
  	my $self = shift;
  	my $req_template = $self->param('req_template') || undef;
  	my $profile_id = $self->session->{'id'} || undef;
  	my $profile = undef;
  	my $alias = $self->config->{'alias'};	

  	return $self->redirect_to('/') if(!defined($profile_id));

  	$profile = $self->app->dbh->select(
		table => 'profiles',
		where => {id => $profile_id},
	)->fetch_hash;

  	utf8::encode($profile->{pfl_content});
  	my $pfl_content_dec = decode_json($profile->{pfl_content});
  
  	if($req_template){
		my $suppliers = $self->app->dbh->select(
			table => 'suppliers',
			column => ['id','caption'],
	)->fetch_all;

	$self->render(
		profile => $profile,
		pfl_content => $pfl_content_dec,
		suppliers => $suppliers,
		template => "requests/tmpl/$alias/$profile->{type}/write/$req_template",
	);

	}else{
		
		$self->render(
			profile_type => $profile->{type},
			certificate_file => $pfl_content_dec->{'certificate_file'},
			template => '/requests/select_template',
		);
  	};
}

sub create {
	my $self = shift;
	my $push_msg = {};
	my $req_content = $self->req->params->to_hash;
 	my $profile_id = $self->session->{id} || undef;
	my $alias = $self->config->{'alias'};

	my $profile = undef;
	my $upload_status = 0;

	return $self->redirect_to('/') if(!defined($profile_id));

	my $file_num = 0;
	foreach my $file (@{$req_content->{upload_files}}){

		foreach my $upload (@{$self->req->every_upload($file)}){

			if($upload && $upload->filename && $upload_status == 0){
				if ($upload->size > 10240000){

					$upload_status = 1;
  					$push_msg = {
                    	type => 'warning',
                    	message => 'Файл '.$upload->filename.' не загружен. Размер файла больше  10Мб.'
                	};

				};

				my $file_ext = ''; #File extention
				my $file_ct = ''; #Content-type header
	            if (exists $self->config->{'content_types'}{$upload->headers->content_type}){
    	            $file_ext = $self->config->{'content_types'}{$upload->headers->content_type};
        	        $file_ct = $upload->headers->content_type;

            	};

				if(!$file_ext){
					$upload_status = 1;
					$push_msg = {
                    	type => 'warning',
	                    message => 'Файл '.$upload->filename.' не загружен. Тип файла не поддерживается.'
    	            };

				};
	
				if ($upload_status == 0){
					my $filename = time."$profile_id".$file_num.'.'.$file_ext;
					$upload->move_to($self->config->{'uploads'}."/requests/$filename");

					push @{$req_content->{$file}}, [$filename, $file_ct];

				};

			};
			$file_num++;
		};
	};

	#Split supplier id, caption from field 'guaranteeing_supplier'
	($req_content->{'supplier_id'}, $req_content->{'guaranteeing_supplier'}) = split (':', $req_content->{'guaranteeing_supplier'});

	$profile = $self->app->dbh->select(
		table => 'profiles',
		columns => ['fuel_name','register_code','pfl_content','type'],
		where => {id => $profile_id},
	)->one;
	
	my $pfl_content = $profile->{pfl_content};
  	utf8::encode($pfl_content);
  
  	if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

	$pfl_content->{fuel_name} = $profile->{fuel_name};
	$pfl_content->{register_code} = $profile->{register_code};
	$pfl_content->{type} = $profile->{type};

	my $req_content_enc = encode_json($req_content);
	my $pfl_content_enc = encode_json($pfl_content);

	if ($upload_status == 0){
	
		$self->app->dbh->insert(
			[
				{
					req_content => $req_content_enc,
					pfl_content => $pfl_content_enc,
					profile_id => $profile_id,
				}
			],
			table => 'requests',
			ctime => 'regdate',
		);
		
		$push_msg = {
			type => 'success',
			message => 'Заявление успешно отправлено',
		};

		$self->app->notify('user', $profile_id, $push_msg->{'message'}, 2);

		$self->flash(
			push_msg => $push_msg,
		);

		$self->redirect_to('/requests/');
		
	};

	my $suppliers = $self->app->dbh->select(
		table => 'suppliers',
		column => ['id','caption'],
	)->fetch_all;
	
	$self->stash(
		push_msg => $push_msg,
	);
	
	$self->render(
		profile => $profile,
		pfl_content => $pfl_content,
		suppliers => $suppliers,
		template => "requests/tmpl/$alias/$profile->{type}/write/$req_content->{req_template}",
	);
}

sub attach_get{
	my $self = shift;
	my $param = $self->req->params->to_hash;
	my $share_link = $self->stash('share_link') || undef;
  	my $profile_id = $self->session->{'id'} || undef;
	my $download = undef; #Downloading params
	my $bugtrace = '';

	if(grep /$param->{'attach_type'}/, ('answer_attach','comment_attach')){#Checking progress attachments type
		$bugtrace = $param->{'attach_type'};
		my $where_condition = {};
		if($profile_id){
			$where_condition= {	
				'progress.id' => $param->{'progress_id'},
				'profile_id' => $profile_id,
			};

		}elsif($share_link){
			$where_condition = {
				'progress.id' => $param->{'progress_id'},
				'share_link' => $share_link,
			};
		}else{
			$where_condition = {'progress.id' => 0}
		};

		my $pgs_content = $self->app->dbh->select(
			table => 'progress',
			column => 'pgs_content',
			where => $where_condition,
			join => ['inner join requests on requests.id = progress.request_id'],
		  )->fetch_hash_one;

		if($pgs_content){
		 	utf8::encode($pgs_content->{'pgs_content'});
        	my $pgs_content_dec = decode_json $pgs_content->{'pgs_content'};

			if($param->{'token'} == $pgs_content_dec->{'token'}){
    	        foreach my $attach (@{$pgs_content_dec->{$param->{'attach_type'}}}){
        	        if($attach->[0] eq $param->{'file'}){
						$download->{'path'} = $self->config('uploads')."/progress/$param->{'file'}";
						$download->{'file-caption'} = $attach->[1];
						utf8::encode($download->{'file-caption'});
						$download->{'content-type'} = $attach->[2];

	                };
    	        };
			};	
		};

	}elsif($share_link){
		if(grep /$param->{'attach_type'}/, ('certificate_file', 'upload_file')){
			my $req_content = $self->app->dbh->select(
				table => 'requests',
				column => ['pfl_content','req_content'],
				where => {'share_link' => $share_link},
			  )->fetch_hash_one;

			if($req_content){

				if($param->{'attach_type'} eq 'certificate_file'){	

					utf8::encode($req_content->{'pfl_content'});
    	    		my $pfl_content_dec = decode_json $req_content->{'pfl_content'};

					if($pfl_content_dec->{'certificate_file'} && $pfl_content_dec->{certificate_file}->[0] eq $param->{'file'}){	
						$download->{'path'} = $self->config('uploads')."/certificates/$param->{'file'}";
						$download->{'file-caption'} = $pfl_content_dec->{certificate_file}->[0]; 
						$download->{'content-type'} = $pfl_content_dec->{certificate_file}->[1];
					};

				}elsif($param->{'attach_type'} eq 'upload_file'){			 	

					utf8::encode($req_content->{'req_content'});
    	    		my $req_content_dec = decode_json $req_content->{'req_content'};

	   	    	    foreach my $upload_file (@{$req_content_dec->{'upload_files'}}){
						foreach my $key (@{$req_content_dec->{$upload_file}}){
	    	    			if($key->[0] eq $param->{'file'}){
								$download->{'path'} = $self->config('uploads')."/requests/$param->{'file'}";
								$download->{'file-caption'} = $key->[0];
								$download->{'content-type'} = $key->[1];

							};
	            		};
					};
				};	
			};
		};
	};

	if($download){
		$self->res->headers->content_type($download->{'content-type'});
		$self->res->headers->content_disposition('attachment; filename="'.$download->{'file-caption'}.'"');
		return $self->reply->file($download->{'path'});

	}else{
		$self->render(
			msg => "Файл не найден. Обратитесь к администратору сайта ".$bugtrace,
			template => 'notification',
		);
	};
}

sub share{
	my $self = shift;
	my $share_link = $self->stash('share_link') || undef;
  	my $alias = $self->config->{'alias'};	
  	my $template_version = $self->param('template_version') || undef;

  	return $self->redirect_to('/') if(!defined($share_link));
  
	my $request = $self->app->dbh->select(
		table => 'requests',
		where => {share_link => $share_link},
  	)->fetch_hash || undef;

  	if(!defined($request)){
	
	return $self->render(
		msg => 'Доступ ограничен или заявление не существует.',
		status_class => 'warning',
		template => 'notification',		
		);
  	};

  	my $req_content = $request->{req_content};
  	utf8::encode($req_content);

  	my $pfl_content = $request->{pfl_content};
  	utf8::encode($pfl_content);
  
  	if (!defined($req_content = decode_json($req_content))) { say 'Error: ', '$json->error' };
  	if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

  	my $statuses = $self->app->dbh->select(
        table => 'statuses',
		columns => ['id','caption'],
  	)->kv;   

	my $progress = $self->app->dbh->select(
		table => 'progress',
		where => {'request_id' => $request->{'id'}},
	)->fetch_hash_all || undef;

	if($progress){
		foreach my $key(@{$progress}){
  			utf8::encode($key->{'pgs_content'});
			$key->{'pgs_content'} = decode_json $key->{'pgs_content'};
		};
	};

  	my $template = $req_content->{req_template};
  	$template = "archive/$template_version/$req_content->{req_template}" if($template_version);

  	$self->render(
		request => $request,
		req_content => $req_content,
		pfl_content => $pfl_content,
		statuses => $statuses,
		progress => $progress,
		template => "requests/tmpl/$alias/$pfl_content->{type}/print/$template"
	);
}

sub comment{
	my $self = shift;
	my $push_msg = {};
	my $share_link = $self->stash('share_link') || '';
	my $comment_content = $self->req->params->to_hash;

	my $request = $self->app->dbh->select(
			table => 'requests',
			column => ['id', 'profile_id'],
			where => {share_link => $share_link}
	)->fetch_hash;	

	if($request){
		my $upload_status = 0;
		my $file_num = 0;
		foreach my $upload (@{$self->req->every_upload('attach')}){

			if($upload && $upload->filename && $upload_status == 0){
				if ($upload->size > 10240000){

					$upload_status = 1;
					$push_msg = {
						type => 'warning',
						message => 'Файл '.$upload->filename.' не загружен. Размер файла больше  10Мб.'
					};

				};
	
				my $file_ext = ''; #File extention
				my $file_ct = ''; #Conent-type header
	            if (exists $self->config->{'content_types'}{$upload->headers->content_type}){
    	            $file_ext = $self->config->{'content_types'}{$upload->headers->content_type};
        	        $file_ct = $upload->headers->content_type;

            	};

	           	if(!$file_ext){
					$upload_status = 1;
					$push_msg = {
						type => 'warning',
						message => 'Файл '.$upload->filename.' не загружен. Тип файла не поддерживается.'
					};

				};
	
				if ($upload_status == 0){
				    my $file_caption = $upload->filename;
	                my $filename = $request->{'id'}.time.$request->{'profile_id'}.$file_num.'.'.$file_ext;
    	            $upload->move_to($self->config->{'uploads'}."/progress/$filename");

		            push @{$comment_content->{'comment_attach'}}, [$filename, $file_caption, $file_ct];
				};
			};
			$file_num++;
		};
	
		if($upload_status == 0){
	        $comment_content->{'token'} = rand(999);
			my $comment_content_enc = encode_json($comment_content);

			$self->app->dbh->insert(
				{
					status_id => $comment_content->{'status_id'},
					request_id => $request->{'id'},
					pgs_content => $comment_content_enc
				},
				ctime => 'regdate',
				table => 'progress',
			);
	
			#update request status
			$self->app->dbh->update(
				{status_id => $comment_content->{'status_id'}},
				table => 'requests',
				where => {id => $request->{'id'}},
			);
		
			$push_msg = {
				type => 'success', 
				message => "Статус заявления ".$self->app->req_fmt($request->{'id'})." обновлен"
			};

			$self->app->notify('user', $request->{'profile_id'}, $push_msg->{'message'});
		};

	};

	$self->flash(
       	push_msg => $push_msg,
    );

	$self->redirect_to('.');

}

sub edit{
	my $self = shift;
	my $request_id = $self->stash('request_id');
	my $alias = $self->config->{'alias'};
	my $push_msg = {};

	my $profile_id = $self->session->{'id'} || undef;
	return $self->redirect_to('/') if(!defined($profile_id));
	
	my $request = $self->app->dbh->select(
		table => 'statuses',
		column => [
			{'statuses' => [qw/type/]},
			{'requests' => [qw/id pfl_content req_content/]},
		],
		join => ['inner join requests on requests.status_id = statuses.id'],
		where => {'requests.id' => $request_id, 'profile_id' => $profile_id, 'statuses.type' => 'edit'},
	)->fetch_hash_one;

	if($request){
		$request->{'id'} = $request->{'requests.id'}; #Patch for template compability (sudo_mode)

		my $req_content = $request->{'requests.req_content'};
		utf8::encode($req_content);

  		my $pfl_content = $request->{'requests.pfl_content'};
  		utf8::encode($pfl_content);

  		if (!defined($req_content = decode_json($req_content))) { say 'Error: ', '$json->error' };
  		if (!defined($pfl_content = decode_json($pfl_content))) { say 'Error: ', '$json->error' };

		my $suppliers = $self->app->dbh->select(
			table => 'suppliers',
			column => ['id','caption'],
		)->fetch_all;

  		my $template = $req_content->{req_template};

  		$self->render(
		    request => $request,
    		req_content => $req_content,
    		pfl_content => $pfl_content,
			suppliers => $suppliers,
    		template => "requests/tmpl/$alias/$pfl_content->{type}/edit/$template",
  		);

	}else{
		$push_msg = {
			'type' => 'warning',
			'message' => 'Заявление запрещено редактировать либо не найдено',
		}; 
		
		$self->flash(push_msg => $push_msg);
		$self->redirect_to('/requests/');

	};
	
}

sub save{
	my $self = shift;
	my $push_msg = {};
	my $request_id = $self->stash('request_id');
	my $req_content = $self->req->params->to_hash;

	my $profile_id = $self->session->{'id'} || undef;
	return $self->redirect_to('/') if(!defined($profile_id));

	if($req_content->{'save_enabled'} eq $self->session->{'csrf_token'}){	
	
		#Split supplier id, caption from field 'guaranteeing_supplier'
	    ($req_content->{'supplier_id'}, $req_content->{'guaranteeing_supplier'}) = split (':', $req_content->{'guaranteeing_supplier'});
	
		#Split upload file values into arrays
		for my $upload_file(@{$req_content->{'upload_files'}}){

			if($req_content->{$upload_file}){

				my @request_attach = ();
			
				if($req_content->{$upload_file} =~ m/:/){ #Check for separator in scalar				
					push @request_attach, $req_content->{$upload_file};
				
				}else{
					@request_attach = @{$req_content->{$upload_file}};

				};

				$req_content->{$upload_file} = ();
				foreach my $key (@request_attach){
					my ($filename, $file_ct) = split(':', $key);

					push @{$req_content->{$upload_file}}, [$filename, $file_ct];

				};
			};
		};

		my $req_content_enc = encode_json($req_content);

		$self->app->dbh->update(
			{'req_content' => $req_content_enc},
			table => 'requests',
			where => {id => $request_id},
		);

		$push_msg = {
			type => 'success',
			message => 'Заявление '.$self->app->req_fmt($request_id).' изменено',
		};
		$self->app->notify('user',$profile_id,$push_msg->{'message'},2);

	}else{
		$push_msg = {
			type => 'warning',
			message => 'Ошибка изменения заявления',
		};
	};
	
	$self->flash(
		push_msg => $push_msg,
	);
	$self->redirect_to('/requests/');
}

1;
