package PersCab::Notification;
use Mojo::Base 'Mojolicious::Controller';

sub notifications{
	my $self = shift;

	my $profile_id = $self->session->{id} || undef;

	return $self->redirect_to('/') if(!defined($profile_id));

	my $notifications = $self->app->dbh->select(
		table => 'notifications',
		where => {
			profile_type => 'user',
			profile_id => $profile_id,
		},
		append => 'order by id desc limit 50',
	)->fetch_hash_all;

	$self->render(notifications => $notifications);
}

1;
