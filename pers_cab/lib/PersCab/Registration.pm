package PersCab::Registration;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw(decode_json encode_json);
use Validator::Custom;
my $vc = Validator::Custom->new;

sub add {
  my $self = shift;
  my $profile_type = '/registration/'.$self->param('type');

  $self->render(
	template => $profile_type,
  );
}

sub create {
	my $self = shift;

	my $profile = $self->req->params->to_hash;
	my $profile_template = '/registration/'.$profile->{type};
	my $rule = [
		fuel_name => {message => 'Укажите полное наименование'} => ['not_blank'],
		register_code => {message => 'Заполните поле без пробелов и тире'} => ['int'],	
		phone => {message => 'Номер мобильного телефона, только цифры'} => ['int', {length => 10}],
		email => {message => 'Укажите корректный адрес почты'} => ['not_blank'],
	];

	my $register_code = $self->app->dbh->select(
		table => 'profiles',
		column => 'true',
		where => {register_code => $profile->{register_code}},	
	)->value;

	$profile->{register_code} = '' if($register_code);

	my $vresult = $vc->validate($profile,$rule);

	if($vresult->is_ok){
	
		while($profile->{password} < 999){
			$profile->{password} = int(rand(9999));
		};

		my $profile_enc = encode_json($profile);

		$self->app->dbh->insert(
			[
				{
				fuel_name => $profile->{fuel_name},
				register_code => $profile->{register_code},
				type => $profile->{type},
				pfl_content => $profile_enc,
				password => $profile->{password},
				},
			],
			table => 'profiles',
				ctime => 'regdate',
		);

		my $profile_id = $self->app->dbh->select(
			column => 'id',
			table => 'profiles',
			where => {register_code => $profile->{register_code}},		
		)->value;

		$self->app->notify('user', $profile_id,'Профиль создан. Пароль: '.$profile->{password}) if $profile_id;
		
		my $push = {
        	type => 'success',
            message => 'Ваш профиль создан. Ожидайте подтверждающее сообщение.',
        };

		$self->flash(
			push_msg => $push,
		);

		return $self->redirect_to('/');

	}else{
		
		my $push_msg = {
        	type => 'danger',
            message => 'Пожалуйста, заполните корректно все поля.',
        };
		
		$self->stash(
			push_msg => $push_msg, 
			messages => $vresult->messages_to_hash,
		) if $vresult->has_invalid;
	};
	
	$self->render(
		profile => $profile,
		template => $profile_template,
  	);
}

1;
