package PersCab;
use Mojo::Base 'Mojolicious';
use DBIx::Custom;

my $config = '';

has 'dbh' => sub {
	my $self = shift;
	
	my $dbi = DBIx::Custom->connect(
			dsn => $config->{dsn},
			user => $config->{user},
			password => $config->{password},
			option => {mysql_enable_utf8 => 1}
	);
	$dbi->execute('SET NAMES utf8');

	return $dbi;
};

# This method will run once at server start
sub startup {
	my $self = shift;

	#Reading config file
	$config = $self->plugin('Config');
	
	$self->mode($config->{mode});
	$self->secrets($config->{secrets});

	$self->helper(notify => sub {
		$_[4] = 0 if(!$_[4]);
		$self->app->dbh->insert(
		[{
			profile_type => $_[1],
			profile_id => $_[2],
			message => $_[3],
			status_code => $_[4],
		}],
		table => 'notifications',
		ctime => 'regdate',	
		); 
	});

	$self->helper(req_fmt => sub {
		sprintf("%04s-Е",$_[1]);
	});

	# Router
	my $r = $self->routes;

	# Normal route to controller
	$r->any('/')->to('profile#auth');

	$r->get('/registration/')->to('registration#add');
	$r->post('/registration/')->to('registration#create');

	$r->get('/profile/')->to('profile#view');
	$r->post('/profile/')->to('profile#update');
	$r->any('/logout/')->to('profile#logout');
	$r->any('/recovery/')->to('profile#recovery');

	$r->get('/requests/')->to('requests#requests');
	$r->get('/requests/new/')->to('requests#add');
	$r->post('/requests/new/')->to('requests#create');
	$r->get('/requests/:request_id/print/')->to('requests#print');
	$r->get('/requests/:request_id/progress/')->to('requests#progress');
	$r->post('/requests/:request_id/progress/')->to('requests#answer');
	$r->get('/requests/:request_id/attach/')->to('requests#attach_get');
	$r->get('/requests/:request_id/edit/')->to('requests#edit');
	$r->post('/requests/:request_id/edit/')->to('requests#save');
	
	$r->get('/share/:share_link/')->to('requests#share');
	$r->post('/share/:share_link/')->to('requests#comment');
	$r->get('/share/:share_link/attach/')->to('requests#attach_get');

	$r->get('/notifications/')->to('notification#notifications');

	$self->hook(before_dispatch => sub {
		my $self = shift;
		$self->req->url->base(Mojo::URL->new($config->{url}));

		}

	) if $self->mode eq 'production';
}

1;
