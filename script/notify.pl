#!/usr/bin/perl -w

use strict;
use warnings;

use lib '/home/eskenergia/perl5/lib/perl5';
use lib '/home/rsk-24/perl5/lib/perl5';
use lib '/home/hosting_3bears/perl5/lib/perl5';

use DBIx::Custom;
use Mojo::JSON qw(decode_json encode_json);
use FindBin qw($Bin);

require "$Bin/pkg/Common.pm";
require "$Bin/pkg/Email.pm";
require "$Bin/pkg/SMS.pm";

my %config = Common::GetConfig();

my $dbi = DBIx::Custom->connect(
            dsn => $config{'dsn'},
            user => $config{'db_user'},
            password => $config{'db_pass'},
            option => {mysql_enable_utf8 => 1}
);

$dbi->execute('SET NAMES utf8');

my $DEV = $config{dev} || undef;
print "Warning! Development mode.\n" if $DEV;

GetUsersMsgList();
GetSuppliersMsgList();
my $message_status = {1 => [], 2 => [], 3 => []};

sub GetUsersMsgList {
	my $message_list = $dbi->select(
		column => [
			{ profiles => ['pfl_content'] },
			{ notifications => [qw/id message/]},
		],
		table => 'notifications',
		where => {
			status_code => 0,
			profile_type => 'user',
		},
		join => 'left join profiles on profiles.id = notifications.profile_id',
	)->fetch_hash_all;
	
	if($message_list){	
		foreach my $message(@{$message_list}){
	        utf8::encode($message->{'profiles.pfl_content'});
    	    my $pfl_content = $message->{'profiles.pfl_content'};
        	if (!defined($pfl_content = decode_json($pfl_content))) { print 'Error: ', '$json->error' };

			$message->{'email'} = $pfl_content->{'email'};
			$message->{'phone'} = $pfl_content->{'phone'};
		};

		PushQuery($message_list) if ($message_list);
	};
};

sub GetSuppliersMsgList {
	my $message_list = $dbi->select(
		column => ['email',
			{ notifications => [qw/id message/]},
		],
		table => 'notifications',
		where => {
			status_code => 0,
			profile_type => 'supplier',
		},
		join => 'left join suppliers on suppliers.id = notifications.profile_id',
	)->fetch_hash_all;
	
	PushQuery($message_list) if ($message_list);
};

sub PushQuery{
	my ($messages) = @_;

	if ($messages){
		foreach my $message (@$messages){
			my $status_code = 0;
	
			if ($message->{'email'}){
				if ($message->{'email'}=~m/^.+@.+\..+$/i){
				
					if ($DEV){
						print "Email ok: $message->{'email'}\n"; 

					}else{;

						$status_code += &Email::SendMessage(
															$config{'mail_api'},
															$config{'mail_token'},
															$message->{'email'},
															$message->{'notifications.message'},
															$config{'email_from'},
															$config{'email_sign'},
															$config{'email_name'},
															$config{'email_client_id'}) ? 0 : 1;
					};
				};

			}else{
				$status_code ++;

			};

			if ($message->{phone}){
				if ($message->{phone}=~m/^\d{10}$/){ #Check phone number
			
					if ($DEV){
						print "Phone ok: $message->{phone}\n"; 

					}else{

						$status_code += &SMS::SendMessage(
															$config{'sms_api'},
															$config{'sms_login'},
															$config{'sms_pass'},
															$message->{'phone'},
															$message->{'notifications.message'},
															$config{'sms_sender'}) ? 0 : 1;
					};
				};

			}else{
				$status_code ++;

			};

			$status_code = $status_code ? $status_code : 3;
			push @{$message_status->{$status_code}}, $message->{'notifications.id'};	
	};

	&UpdateMsgStatus;
	};
};

if ($DEV){

	foreach my $status_code (keys %{$message_status}){
		foreach my $keys (@{$message_status->{$status_code}}){
			print "$status_code = $keys\n";
		};
	};
};

sub UpdateMsgStatus {

	foreach my $status_code (keys %{$message_status}){
		$dbi->update(
			{status_code => $status_code},
			where => {id => $message_status->{$status_code}},
			table => 'notifications',
			mtime => 'regdate',
		);
	};
}

1;
