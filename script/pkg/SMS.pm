package SMS;

use strict;
use utf8;

use Mojo::UserAgent;
use Mojo::Util qw(url_escape url_unescape);

sub SendMessage{

	my ($sms_api,$login,$pass,$rcpt,$message,$sms_sender) = @_;

	utf8::encode($message);
	$message = url_escape $message;

	my $ua = Mojo::UserAgent->new();

	my $tx = $ua->get("$sms_api/?user=$login&pass=$pass&text=$message&dadr=+7$rcpt&sadr=$sms_sender");

	my $err = $tx->resume->{res}->is_error;
	
	print $err;
	return $err;

};

1;
