package Email;

use strict;
use utf8;

use Mojo::UserAgent;
use Mojo::JSON qw(decode_json encode_json);

sub SendMessage{

	my ($mail_api,$mail_token,$rcpt,$message,$email_from,$email_sign,$email_name,$email_client_id) = @_;

	utf8::decode($email_sign);
	utf8::decode($email_name);

	my $subject = 'Новое уведомление';
	my $template = "Получено новое уведомление в личном кабинете.\n".$message."\n\n".$email_sign;

	my $ua = Mojo::UserAgent->new();
	my $tx = $ua->post($mail_api => {'Authorization' => 'Bearer '.$mail_token} => json => {
		from_email => $email_from,
		from_name => $email_name,
		to => $rcpt,
		subject => $subject,
		html => "<pre>$template</pre>",
		text => $template,
		payment => 'credit',
		smtp_headers => {
			"Client-Id" => $email_client_id,
		},
	})->res->body;

	if (!defined($tx = decode_json($tx))) { print 'Error: ', '$json->error' };

	my $result = $tx->{ErrorCode};
	return $result;

};

1;
